<?php
 /**
  * Class Config\Autoload | config/autoload.php
  * 
  * This file contain an Autoload Class to autoload files when called
  *
  * @author  Rafael Figueredo <refo44@gmail.com>
  * @version  1.0.0
  * @since 1.0.0
  * @package  MVC Entrevista Bitmall
  * @subpackage Config
  */
 
	namespace Config;
 	
 		require_once 'vendor/autoload.php';
 		require_once ".env";
	
	/**
	 * Class Autoload
	 * 
	 * If a class is called then autoload the file
	 */
	class Autoload{

		/**
		 * Autoload::run() 
		 * 
		 * This static method execute the autoload
		 */
		public static function run(){
			spl_autoload_register(function($class){
				$ruta = str_replace("\\", "/", $class ) . ".php";
				$ruta = strtolower($ruta);
				if(is_readable($ruta)){

				 	require_once($ruta); 
				}
			});
		}

	}