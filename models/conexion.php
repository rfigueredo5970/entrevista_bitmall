<?php  
 /**
  * Class Models\Conexion | models/conexion.php
  *  
  * This file contain a Singleton Class to create Mysql Database conexion and handle queries to Database
  *
  * @author  Rafael Figueredo <refo44@gmail.com>
  * @version  1.0.0
  * @since 1.0.0
  * @package  MVC Entrevista Bitmall
  * @subpackage \Models
  * @uses PDO
  * @used-by Models\Model 
  */
 
namespace Models;

use \PDO as PDO;
use \PDOException;

/**
 * Class Models\Model
 * 
 * This Singleton class create a Mysql Database conexion and handle queries to Database
 */
class Conexion
{
	/**
	 * An instance of the Models\Conexion singleton class.
	 * @var Models\Conexion $instance  
	 */
	public static $instance;

	/**
	 * The last error message if any.
	 * @var string|null $error  
	 */
	public $error;

	/**
	 * An array with data obtained from the .env file.
	 * @var array $data
	 */
	private $data;

	/**
	 * An instance of the PDO class.  
	 * @var PDO|PDOException $con  
	 */
	private $con;

	/**
	 * An object representing a SQL query statement.
	 * @var PDOStatement $stmt 
	 */
	private $stmt;

	/**
	 * __construct
	 * 
	 * The method __construct() is declared as protected to prevent new instances of this singleton class via the new operator
	 */
	protected function __construct()
	{	
		// Set data
			$data = array(
				"host" => $_ENV['DB_HOST'],
				"user" => $_ENV['DB_USER'],
				"password" => $_ENV['DB_PASSWORD'],
				"db" => $_ENV['DB_DATABASE'],
				"charset" =>$_ENV['DB_CHARSET']
			);

		// Set Database Source Name (DNS)
        $dsn = 'mysql:host=' . $_ENV['DB_HOST'] . ';dbname=' . $_ENV['DB_DATABASE'] . ';charset=' . $_ENV['DB_CHARSET'];

    	// Set options
		$options = array(
    		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
		);

        // Create a new PDO instance
        try
        {        	
            $this->con = new PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASSWORD'], $options);
            $this->con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );           
        }
        // Catch database errors
        catch(PDOException $e)
        {
            $this->error = $e->getMessage();
            header("HTTP/1.0 500 Internal Server Error");
            echo "ERROR 500 - Database conexion error:" . $e->getMessage();
        }
       	// Catch any errors
        catch(Exception $e)
        {
            $this->error = $e->getMessage();
            echo "ERROR 500 - Database conexion error:" . $e->getMessage();
        }


	}
	
	/**
	 * __clone
	 *  
	 * The magic method __clone() is declared as protected to prevent cloning of an instance of the class via the clone operator
	 */
	protected function  __clone()
	{
		//
	}

	/**
	 * __wakeup
	 *  
	 * The magic method __wakeup() is declared as protected to prevent unserializing of an instance of the class via the global function unserialize() .
	 */
	protected function  __wakeup()
	{
		// 
	}

	/**
	 * getInstance
	 * 
	 * This method return an instance of Conexion or create a new instance if there aren't any 
	 * 
	 * @return Models\Conexion Returns an instance of the Models\Conexion singleton class
	 */
	public static function getInstance()
 	{
    	if (self::$instance == null)
    	{

      		self::$instance = new Conexion();
    	}
 
    		return self::$instance;
  	}

  	/**
  	 * query
  	 * 
  	 * This method hold the SQL query statement in the $stmt variable and prepare it to bind the values
  	 * 
  	 * @param  string $query SQL query statement
  	 * @return PDOStatement|PDOException|FALSE If the database server successfully prepares the statement returns a PDOStatement object        
  	 */
	public function query($query)
	{
   

			try
			{

				return $this->stmt = $this->con->prepare($query);
			}
			// Catch database errors
        	catch(PDOException $e)
        	{
            	$this->error = $e->getMessage();
            	header("HTTP/1.0 500 Internal Server Error");
            	echo "ERROR 500 - Database conexion error:" . $e->getMessage();
            	die();
        	}
       		// Catch any errors
        	catch(Exception $e)
        	{
            	$this->error = $e->getMessage();
            	header("HTTP/1.0 500 Internal Server Error");
            	echo "ERROR 500 - General error:" . $e->getMessage();
            	die();
        	}
	}

	/**
	 * bind
	 * 
	 * This method bind the inputs with the placeholder value used in the SQL statement prepared in the $stmt variable
	 * 
	 * @param  mixed $param placeholder value used in the SQL statement (Example :name).
	 * @param  mixed $value  the actual value that we want to bind to the placeholders.
	 * @param  int|null $type PDO::PARAM_* constant datatype of the parameter ( Example: PDO::PARAM_STR).
	 * @return boolean Returns TRUE on success or FALSE on failure. 
	 */
	public function bind($param, $value, $type = null)
	{
	   if (is_null($type)) {
	       switch (true) {
	           case is_int($value):
	               $type = PDO::PARAM_INT;
	               break;
	           case is_bool($value):
	               $type = PDO::PARAM_BOOL;
	               break;
	           case is_null($value):
	               $type = PDO::PARAM_NULL;
	               break;
	           default:
	               $type = PDO::PARAM_STR;
	       }
	   }
	   return $this->stmt->bindValue($param, $value, $type);
	}

	/**
	 * execute
	 * 
	 * This method executes the prepared statement.
	 * It uses the PDOStatement::execute PDO method of the PDO class. 
	 * 
	 * @return boolean TRUE on success or FALSE on failure.
	 */
	public function execute()
	{
    	return $this->stmt->execute();
	}

	/**
	 * resultset
	 * 
	 * This method returns an array of the result set rows
	 * First run the execute method, then 
	 * uses the PDOStatement::fetchAll PDO method to return the results.
	 * 
	 * @return array results of the query to the Database 
	 */
	public function resultset()
	{
    	$this->execute();
    	return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * single
	 * 
	 * This method simply returns a single record from the database.
	 * First run the execute method, then 
	 * uses the PDOStatement::fetch PDO method to return the single result.
	 * 
	 * @return array single result of the query to the Database
	 */
	public function single()
	{
    	$this->execute();
    	return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * rowCount
	 * 
	 * This method returns the number of affected rows from the previous delete, update or insert statemen
	 * It uses the PDO method PDOStatement::rowCount.
	 * 
	 * @return int number of rows
	 */
	public function rowCount()
	{
    	return $this->stmt->rowCount();
	}

	/**
	 * lastInsertId
	 * 
	 * This method returns the last inserted Id as a string.
	 * It uses the PDO method PDO::lastInsertId.
	 * 
	 * @return string last inserted Id.
	 */
	public function lastInsertId()
	{
    	return $this->con->lastInsertId();
	}

	/**
	 * beginTransaction
	 * 
	 * This method  begin a transaction to Database
	 * 
	 * @return boolean TRUE on success or FALSE on failure. 
	 */
	public function beginTransaction()
	{
    	return $this->con->beginTransaction();
	}

	/**
	 * endTransaction
	 * 
	 * This method end a transaction and commit the changes to Database.
	 * 
	 * @return boolean TRUE on success or FALSE on failure. 
	 */
	public function endTransaction()
	{
    	return $this->con->commit();
	}

	/**
	 * cancelTransaction
	 * 
	 * This method cancel a transaction and roll back your changes
	 * 
	 * @return boolean TRUE on success or FALSE on failure. 
	 */
	public function cancelTransaction()
	{
    	return $this->con->rollBack();
	}
}



