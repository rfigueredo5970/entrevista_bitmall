<?php
/**
 * Class Models\Model | models/model.php
 * 
 * 
 * This file contain a Model class for the MVC.
 *
 * @author  Rafael Figueredo <refo44@gmail.com>
 * @version  1.0.0
 * @since 1.0.0
 * @package  MVC Entrevista Bitmall
 * @subpackage \Models
 * @uses Models\Conexion
 * 
 */
 
 namespace Models;

 use Models\Conexion as Conexion;
 use Carbon\Carbon as Carbon;

/**
 * Class Models\Model
 * 
 * This class is a Model for the MVC.
 */
	class Model
	{

		/**
		 * An instance of the Models\Conexion singleton class. 
		 * @var Models\Conexion $con
		 */
		private $con;

		/**
		 * The id number of the user.
		 * @var int $id 
		 */
		private $id;

		/**
		 * The name of the user.
		 * @var string $name 
		 */
		private $name;

		/**
		 * The type of the document of identity of the user.
		 * @var int $document_type
		 */
		private $document_type;

		/**
		 * The number of document of the user.
		 * @var string $document
		 */
		private $document;

		/**
		 * The cellphone number of the user.
		 * @var int  $cellphone
		 */
		private $cellphone;

		/**
		 * The email of the user.
		 * @var string $email
		 */
		private $email;

		/**
		 * The Date of birthday of the user as a date string (YYYY - MM - DD).
		 * @var Date $birthday
		 */
		private $birthday;

		/**
		 * The id of the status (active or inactive) of the user.
		 * @var int $status
		 */
		private $status;

		/**
		 * The id of the city of the user.
		 * @var int $city
		 */
		private $city;

		/**
		 * __construct
		 * 
		 * This is the constructor method of the Models\Model class.
		 */
		public function __construct()	
		{
			$this->con = Conexion::getInstance();			
		}

		/**
		 * set
		 * 
		 * The setter method for the attributes of the Models\Model class.
		 * @param string $atribute the name of the atribute.
		 * @param string $content  the value to set on the atribute.
		 */
		public function set($atribute,$content)
		{
			if ($atribute != "con") 
			{
				$this->$atribute = $content;
			}
		}

		/**
		 * get
		 * 
		 * The getter method to obtain the values for the attributes of the Models\Model class. 
		 * @param  string $atribute The name of the atribute.
		 * @return mixed Returns the value or returns false, 
		 */
		public function get($atribute)
		{
			if ($atribute != "con") 
			{
				return $this->$atribute;
			}else
			{
				return false;
			}
		}

		/**
		 * index
		 * 
		 * This method is to obtain a list of all the users from the Database
		 * 
		 * @return array  List of all users
		 */
		public function index()
		{

			$sql = "SELECT usuarios_id, usuarios_nombre, usuarios_tipo_documento, usuarios_documento, usuarios_celular, usuarios_correo, usuarios_fecha_nacimiento, usuarios_estado, usuarios_ciudad_residencia FROM usuarios";

			$this->con->query($sql);

			return $this->con->resultset();
		}

		/**
		 * add
		 * 
		 * This method create a new user in the Database
		 * @return string|boolean on sucess returns the id of the new user as a string or returns false
		 */
		public function add()
		{
			
			$sql = "INSERT INTO usuarios (usuarios_nombre, usuarios_tipo_documento, usuarios_documento, usuarios_celular, usuarios_correo, usuarios_fecha_nacimiento, usuarios_estado, usuarios_ciudad_residencia) VALUES (:name, :document_type, :document, :cellphone, :email, :birthday,:status,:city)";

			$this->con->query($sql);

			//Bind values to placeholders
			$this->con->bind(':name', $this->name);
			$this->con->bind(':document_type', $this->document_type);
			$this->con->bind(':document', $this->document);
			$this->con->bind(':cellphone', $this->cellphone);
			$this->con->bind(':email', $this->email);
			$this->con->bind(':birthday', $this->birthday);
			$this->con->bind(':status', $this->status);
			$this->con->bind(':city', $this->city);

			$this->con->execute();

			return $this->con->lastInsertId();

		}

		/**
		 * delete
		 * 
		 * This method deletes an user from the Database.
		 * @param  int $id the id number of the user to delete.
		 * @return boolean  returns true on success or false on failure.
		 */
		public function delete($id = NULL)
		{

			$id = is_null($id) ?  $this->id : $id;

			$sql = "DELETE FROM usuarios WHERE usuarios_id = :id";
			$this->con->query($sql);

			//Bind values to placeholders
			$this->con->bind(':id', $id);

			return $this->con->execute();
		}

		/**
		 * update
		 * 
		 * This method updates an user from the Database.
		 * @param  int $id the id number of the user to update.
		 * @return boolean  returns true on success or false on failure.
		 */
		public function update($id = NULL)
		{

			$id = is_null($id) ?  $this->id : $id;

			$sql = "UPDATE usuarios SET usuarios_nombre = :name, usuarios_tipo_documento = :document_type, usuarios_documento = :document, usuarios_celular = :cellphone, usuarios_correo = :email, usuarios_fecha_nacimiento = :birthday, usuarios_estado = :status, usuarios_ciudad_residencia = :city WHERE usuarios_id = :id";

			$this->con->query($sql);

			//Bind values to placeholders
			$this->con->bind(':id', $id);
			$this->con->bind(':name', $this->name);
			$this->con->bind(':document_type', $this->document_type);
			$this->con->bind(':document', $this->document);
			$this->con->bind(':cellphone', $this->cellphone);
			$this->con->bind(':email', $this->email);
			$this->con->bind(':birthday', $this->birthday);
			$this->con->bind(':status', $this->status);
			$this->con->bind(':city', $this->city);

			return $this->con->execute();
		}

		/**
		 * view
		 * 
		 * This method returns an array with an user.
		 * @param  int $id the id number of the user to show.
		 * @return array An array with the user information.
		 */
		public function view($id = NULL)
		{

			$id = is_null($id) ?  $this->id : $id;

			$sql = "SELECT usuarios_id, usuarios_nombre, usuarios_tipo_documento, usuarios_documento, usuarios_celular, usuarios_correo, usuarios_fecha_nacimiento, usuarios_estado, usuarios_ciudad_residencia FROM usuarios WHERE usuarios_id = :id";

			$this->con->query($sql);

			//Bind values to placeholders
			$this->con->bind(':id', $id);

			return $this->con->single();
		}

		/**
		 * countItems
		 * 
		 * This method count the items retrieved on the last query to Database.
		 * @return int The number of items.
		 */
		public function countItems()	
		{
			return $this->con->rowCount();
		}

		/**
		 * citiesList
		 * 
		 * This method returns a list of the cities on the Database
		 * @return [type] [description]
		 */
		public function citiesList()
		{

			$sql = "SELECT ciudades_id, ciudades_nombre FROM ciudades";

			$this->con->query($sql);

			return $this->con->resultset();
		}

		/**
		 * statusList
		 * 
		 * This method returns a list of the types of status of the user on the Database
		 * @return [type] [description]
		 */
		public function statusList()
		{

			$sql = "SELECT estados_id, estados_descripcion FROM estados";

			$this->con->query($sql);

			return $this->con->resultset();
		}

		/**
		 * documentTypesList
		 * 
		 * This method returns a list of the document types on the Database
		 * @return [type] [description]
		 */
		public function documentTypesList()
		{

			$sql = "SELECT tipos_documento_id, tipos_documento_descripcion FROM tipos_documentos";

			$this->con->query($sql);

			return $this->con->resultset();
		}

	}
