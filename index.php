<?php
 /**
  *  Home | index.php
  * 
  * This is the home file of the application
  *
  * @author  Rafael Figueredo <refo44@gmail.com>
  * @version  1.0.0
  * @since 1.0.0
  * @package  MVC Entrevista Bitmall
  * @uses Models\Model
  * 
  */
 
	define("ROOT", realpath(dirname(__FILE__)) . "/" );
	define("URL","http://localhost/rafael_figueredo/");
	
	require_once "config/autoload.php";
	
	Config\Autoload::run();

	use Models\Model;
	use Carbon\Carbon as Carbon;

	$m = new Model();
	$m->index();
	$i = $m->countItems();
	$m->set("name","CARLOS" . $i);
	$m->set("document_type",1);
	$m->set("document","10000000000" . $i);
	$m->set("cellphone",2334456789);
	$m->set("email","email_" . $i . "@example.com");
	$m->set("birthday",Carbon::now()->setDate(1975, 5, 21)->toDateString());
	$m->set("status",1);
	$m->set("city",1);
	echo "<br>" . $id = $m->add();

	$m->set("id",$id);
	$data = array();
	$data = $m->view();

	var_dump($data);
	echo "<hr>";
	echo "N rows: " . $m->countItems() . "<br>";
	echo "id:" . $id;  
	echo "<hr>";


	$m->set("status",2);
	$m->set("city",2);
	echo "<br> Update:" . $m->update();
	echo "<br>" . $m->get("id");
	
	$m->set("id",$id);
	$data = array();
	$data = $m->view();

	var_dump($data);
	echo "<hr>";
	echo "N rows: " . $m->countItems() . "<br>";
	echo "id:" . $id;  
	echo "<hr>";

	$m->delete($id);
